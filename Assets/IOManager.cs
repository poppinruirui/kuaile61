﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Net;
using System.Xml;
using UnityEngine.UI;

public class IOManager : MonoBehaviour {

    void Awake()
    {

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void DownLoad(string url, string path)
    {
        /*
        WebClient client = new WebClient();

        string URLAddress = url;

        string receivePath= path;

        client.DownloadFile(URLAddress, receivePath );
        client.Dispose();
        */
        ///  创建WebClient实例 
        WebClient myWebClient = new WebClient();
        myWebClient.Credentials = CredentialCache.DefaultCredentials;


        // 要上传的文件 
        FileStream fs = new FileStream(url, FileMode.Open, FileAccess.Read);
        BinaryReader r = new BinaryReader(fs);
        byte[] postArray = r.ReadBytes((int)fs.Length);
        Stream postStream = myWebClient.OpenWrite(path, "PUT");


        // 使用UploadFile方法可以用下面的格式
        // myWebClient.UploadFile(uriString,"PUT",fileNamePath);

        if (postStream.CanWrite)
        {
            postStream.Write(postArray, 0, postArray.Length);
            postStream.Close();
            fs.Dispose();
        }
        else
        {
            postStream.Close();
            fs.Dispose();
        }
        myWebClient.Dispose();
    }

    public static  void Upload (string fileNamePath, string uriString)
    {
        ///  创建WebClient实例 
        WebClient myWebClient = new WebClient();
        myWebClient.Credentials = CredentialCache.DefaultCredentials;


        // 要上传的文件 
        FileStream fs = new FileStream(fileNamePath, FileMode.Open, FileAccess.Read);
        BinaryReader r = new BinaryReader(fs);
        byte[] postArray = r.ReadBytes((int)fs.Length);
        Stream postStream = myWebClient.OpenWrite(uriString, "PUT");


        // 使用UploadFile方法可以用下面的格式
        // myWebClient.UploadFile(uriString,"PUT",fileNamePath);

        if (postStream.CanWrite)
        {
            postStream.Write(postArray, 0, postArray.Length);
            postStream.Close();
            fs.Dispose();
        }
        else
        {
            postStream.Close();
            fs.Dispose();
        }
        myWebClient.Dispose();
    }

    public static XmlDocument LoadXmlFile( string szFileName, ref  XmlNode root )
    {
        XmlDocument myXmlDoc = new XmlDocument();
        myXmlDoc.Load(szFileName);
        root = myXmlDoc.SelectSingleNode("root");
        return myXmlDoc;
    }

    public static  void CreateNode(XmlDocument xmlDoc,XmlNode parentNode,string name,string value)  
    {  
        XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);  
        node.InnerText = value;  
        parentNode.AppendChild(node);  
    }

    public static  void UpdateDropdownView( Dropdown dropdownItem,  List<string> showNames)
    {
        dropdownItem.options.Clear();
        Dropdown.OptionData tempData;
        for (int i = 0; i < showNames.Count; i++)
        {
            tempData = new Dropdown.OptionData();
            tempData.text = showNames[i];
            dropdownItem.options.Add(tempData);
        }
        //dropdownItem.captionText.text = showNames[0];
    }

    public static  void CreateXmlFile( string szFileName )
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
        xmlDoc.AppendChild(node);
        XmlNode root = xmlDoc.CreateElement("root");
        xmlDoc.AppendChild(root);
        XmlNode nodeCommon = xmlDoc.CreateElement("common");
        root.AppendChild(nodeCommon);
        xmlDoc.Save(szFileName);
    }

    public static  string GenerateServerName( string szRoomName )
    {
        string szServerName = "\\\\192.168.31.1\\tddownload" + "/" + szRoomName + ".xml";
        return szServerName;
    }

    public static string GenerateServerPath()
    {
        string szServerName = "\\\\192.168.31.1\\tddownload";
        return szServerName;
    }

    public static  string GenerateClientName( string szRoomName )
    {
        string szClientName = Application.streamingAssetsPath + "/" + szRoomName + ".xml";
        return szClientName;
    }

    public static  string GenerateClientPath()
    {
        string szClientName = Application.streamingAssetsPath;
        return szClientName;
    }
}
