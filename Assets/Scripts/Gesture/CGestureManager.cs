﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CGestureManager : MonoBehaviour {

    public static CGestureManager s_Instance = null;

    
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    /// <summary>
    /// /data
    /// </summary>
    public int m_nAvailableNum = 5;

    public Sprite[] m_aryGestures;
    public Sprite m_sprTerminalWithTail;
    public Sprite m_sprTerminalWithoutTail;

    /// <summary>
    /// UI
    /// </summary>
    public Button _btnToggle;

    public InputField _inputGestureNumOneTime;
    public InputField _inputColdDown;
    public InputField _inputLastTime;
    public InputField _inputMaxEquippedNum;
    public InputField _inputSmallColdDown;
    public Image _imgColdDown;
    public Image _imgColdDownBg;

    public GameObject m_panelGesturesSystem;
    public GameObject m_goContainerBoughtGestures;
    public GameObject m_goContainerSelectedGestures;
    public GameObject m_goContainerPopo;

    public GameObject m_preGesture;
    public GameObject m_preGestureNotUI;
    public GameObject m_preGesturePopo;
    public GameObject m_preGesturePopoCenter;

    public float m_fBoughtGesturesInterval = 80;
    public float m_fSelectedGesturesInterval = 80;
    public float m_fCastedGesturesInterval = 80;

    public float m_fBoughtGesturesScale = 0.4f;
    public float m_fSelectedGesturesScale = 0.4f;
    public float m_fCastedGesturesScale = 0.4f;

    public float m_fUnitWidth = 61f; // 单位宽度
    public float m_fBgOffsetY = 0.085f;

    public float m_fWidthPerCharaqcter = 0.242f;

    public float m_fGenstureOriginalWidth = 220f;
    public float m_fLeftAndRightOffset = 35f;

    

    public int m_nMaxSelectedNum = 5;
    public float m_fColdDown = 10f;
    public float m_fSmallColdDown = 1f;
    public float m_fPopoLastTime = 5f;


    public float m_fLastCastGestureTime = 0f;



    public float m_fOffestX = 0;
    public float m_fOffestY = 0;


    List<CGesture> m_lstSelcted = new List<CGesture>();

    public enum eGesturePopoDir
    {
        left_top,
        right_top,
        left_bottom,
        right_bottom,
    };

    public eGesturePopoDir m_eDir = eGesturePopoDir.left_bottom;



    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
      

     
     
    }
	
	// Update is called once per frame
	void Update () {

        SmallColdDownLoop();
        BigColdDownLoop();
    }

    public Sprite GetGestureSpriteById( int nId )
    {
        if ( nId < 0 || nId >= m_aryGestures.Length)
        {
            return null;
        }
        return m_aryGestures[nId];
    }

    void InitBoughtGestures()
    {
        if (m_nAvailableNum > m_aryGestures.Length)
        {
            m_nAvailableNum = m_aryGestures.Length;
        }
        int nNum = m_nAvailableNum;
        for ( int i = 0; i < m_nAvailableNum; i++ )
        {
            CGesture gesture = NewGesture();
            gesture.SetSprite( GetGestureSpriteById( i ) );
            vecTempPos.x = -i * m_fBoughtGesturesInterval;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            gesture.transform.SetParent(m_goContainerBoughtGestures.transform);
            gesture.SetLocalPos(vecTempPos);
            gesture.SetScale(m_fBoughtGesturesScale);
            gesture.SetID( i );
        }

        int nMainBgBarScale = nNum + 1;
        float fMianBgOffset = ( -nMainBgBarScale + 1 ) * m_fBoughtUnitWidht / 2f;
        vecTempPos.x = fMianBgOffset;
        vecTempPos.y = 0;
        vecTempPos.z = 0;
        m_goBoughtListMain.transform.localPosition = vecTempPos;
        vecTempScale.x = nMainBgBarScale;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        m_goBoughtListMain.transform.localScale = vecTempScale;

        vecTempPos.x = -nMainBgBarScale * m_fBoughtUnitWidht;
        vecTempPos.y = 0;
        vecTempPos.z = 0;
        m_goBoughtListLeft.transform.localPosition = vecTempPos;
    }

    List<CGesture> m_lstRecycledGestures = new List<CGesture>();
    public CGesture NewGesture( bool bRaycast = true, bool bUI = true )
    {
        CGesture gesture = null;
        if (m_lstRecycledGestures.Count > 0)
        {
            gesture = m_lstRecycledGestures[0];
            gesture.gameObject.SetActive( true );
            m_lstRecycledGestures.RemoveAt(0);
        }
        else
        {
            gesture = GameObject.Instantiate(m_preGesture).GetComponent<CGesture>();
        }

        gesture.m_bUI = bUI;
        gesture.SetRaycastEnable(bRaycast);

        return gesture;
    }

    public void DeleteGesture( CGesture gesture )
    {
        gesture.gameObject.SetActive(false);
        m_lstRecycledGestures.Add(gesture );
    }


    List<CGesturePopo> m_lstRecycledGesturePopos = new List<CGesturePopo>();
    public CGesturePopo NewGesturePopo()
    {
        CGesturePopo popo = null;
        if (m_lstRecycledGesturePopos.Count > 0)
        {
            popo = m_lstRecycledGesturePopos[0];
            popo.gameObject.SetActive(true);
            m_lstRecycledGesturePopos.RemoveAt(0);
        }
        else
        {
            popo = GameObject.Instantiate(m_preGesturePopo).GetComponent<CGesturePopo>();
        }

        return popo;
    }

    public void DeleteGesturePopo(CGesturePopo popo)
    {
        popo.gameObject.SetActive(false);
        m_lstRecycledGesturePopos.Add(popo);
    }

    List<GameObject> m_lstRecycledPopoCenter = new List<GameObject>();
    public GameObject NewPopoCenter()
    {
        GameObject center = null;
        if (m_lstRecycledPopoCenter.Count > 0)
        {
            center = m_lstRecycledPopoCenter[0];
            center.SetActive( true );
            m_lstRecycledPopoCenter.RemoveAt(0);
        }
        else
        {
            center = GameObject.Instantiate(m_preGesturePopoCenter);
        }
        return center;
    }

    public void DeletePopoCenter( GameObject center )
    {
        center.SetActive(false );
        m_lstRecycledPopoCenter.Add( center );
    }

    bool m_bShowing = false;
    public void OnClickButton_Popo()
    {
        if ( m_bShowing )
        {
            m_fLastCastGestureTime = Main.GetTime();

            CastSelectedGestures( eGesturePopoDir.right_bottom );
            SetMainPanelVisible( false );
        }
        else
        {
            // 先判断ColdDown结束没有
            if (!IsBigColdDownEnd())
            {
                Main.s_Instance.g_SystemMsg.SetContent("手势ColdDown还没结束");
                return;
            }
            SetMainPanelVisible( true );
        }

      
    }

 
    public void AddGestureToSelectedList( int nGestureId )
    {
        if (m_lstSelcted.Count >= m_nMaxSelectedNum)
        {
            return;
        }

        CGesture gesture = NewGesture( false );
        gesture.SetSprite(GetGestureSpriteById(nGestureId));

        vecTempPos.x = (-m_nMaxSelectedNum + m_lstSelcted.Count + 1) * m_fSelectedGesturesInterval;
        vecTempPos.y = 0;
        vecTempPos.z = 0;
        gesture.transform.SetParent(m_goContainerSelectedGestures.transform);
        gesture.SetLocalPos(vecTempPos);
        gesture.SetScale(m_fSelectedGesturesScale);
        gesture.SetID(nGestureId);
        m_lstSelcted.Add(gesture);

        BeginSmallColdDown();

        if (m_lstSelcted.Count >= m_nMaxSelectedNum)
        {
            CastSelectedGestures();
        }
    }

    float m_fSmallColdDownTimeLeft = 0;
    void BeginSmallColdDown()
    {
        SetColdDowmImageVisible( true );
        m_fSmallColdDownTimeLeft = m_fSmallColdDown;
    }

    void SmallColdDownLoop()
    {
        if (m_fSmallColdDownTimeLeft <= 0f)
        {
            return;
        }
        m_fSmallColdDownTimeLeft -= Time.deltaTime;
        if (m_fSmallColdDownTimeLeft <= 0f)
        {
            EndSmallColdDown();
        }

        _imgColdDown.fillAmount = (m_fSmallColdDown - m_fSmallColdDownTimeLeft) / m_fSmallColdDown;
    }

    void EndSmallColdDown()
    {
        m_fSmallColdDownTimeLeft = 0f;
        SetColdDowmImageVisible( false );
        CastSelectedGestures(eGesturePopoDir.right_bottom);
    }

    public bool IsBigColdDownEnd()
    {
        return m_fBigColdDownTimeLeft <= 0;
    }


    void BeginBigColdDown()
    {
        EndSmallColdDown();
        SetColdDowmImageVisible(true);
        m_fBigColdDownTimeLeft = m_fColdDown;
    }

    float m_fBigColdDownTimeLeft = 0f;
    void BigColdDownLoop()
    {
        if (m_fBigColdDownTimeLeft <= 0)
        {
            return;
        }
        m_fBigColdDownTimeLeft -= Time.deltaTime;
        if (m_fBigColdDownTimeLeft <= 0)
        {
            EndBigColdDown();
        }

        _imgColdDown.fillAmount = (m_fColdDown - m_fBigColdDownTimeLeft) / m_fColdDown;
    }

    void EndBigColdDown()
    {
        m_fBigColdDownTimeLeft = 0f;
        SetColdDowmImageVisible( false );
    }

    void SetColdDowmImageVisible( bool bVisible )
    {
        _imgColdDown.gameObject.SetActive(bVisible);
        _imgColdDownBg.gameObject.SetActive(bVisible);
    }

    List<int> s_lstCastingGestureId = new List<int>();
    public void CastSelectedGestures(eGesturePopoDir eDir = eGesturePopoDir.right_bottom)
    {
        int nNum = m_lstSelcted.Count;
        if (nNum == 0)
        {
            return;
        }

        s_lstCastingGestureId.Clear();
        for (int i = 0; i < m_lstSelcted.Count; i++)
        {
            s_lstCastingGestureId.Add(m_lstSelcted[i].GetID());
        }

        Main.s_Instance.m_MainPlayer.CastGestures(ref s_lstCastingGestureId);

        for ( int i = 0; i < m_lstSelcted.Count; i++ )
        {
            DeleteGesture(m_lstSelcted[i]);
        }
        m_lstSelcted.Clear();

        BeginBigColdDown();
        SetMainPanelVisible( false );
    }

    public void SetMainPanelVisible( bool bVisible )
    {
        m_panelGesturesSystem.SetActive(bVisible);
        m_bShowing = bVisible;
    }

    public void CastGestures( ref List<int> lstGesturesId, string szPlayerName, Ball ball, float fOffsetX, float fOffsetY )
    {
        eGesturePopoDir eDir = eGesturePopoDir.left_bottom;
        if ( fOffsetX > 0 && fOffsetY > 0 )
        {
            eDir = eGesturePopoDir.right_top; 
        }
        else if (fOffsetX > 0 && fOffsetY < 0)
        {
            eDir = eGesturePopoDir.right_bottom;
        }
        else if (fOffsetX < 0 && fOffsetY > 0)
        {
            eDir = eGesturePopoDir.left_top;
        }
        else if (fOffsetX < 0 && fOffsetY < 0)
        {
            eDir = eGesturePopoDir.left_bottom;
        }


        CGesturePopo popo = NewGesturePopo();
        popo.transform.SetParent(m_goContainerPopo.transform);
        popo.CastGestures(ref lstGesturesId, eDir, szPlayerName + "：");
        popo.SetBall( ball );
        popo.BallLoop();
    }

    public CGesturePopo _testPopo;
    void Test()
    {

    }

    ///////////////////// 以下为界面部分
    public GameObject m_goBoughtListLeft;
    public GameObject m_goBoughtListMain;
    public float m_fBoughtUnitWidht = 61f;

    public void OnInputValueChanged_GestureNumOneTime()
    {
        int val = 1;
        if ( !int.TryParse( _inputGestureNumOneTime.text, out val ) )
        {
            val = 1;
        }
        m_nMaxSelectedNum = val;
    }

    public void OnInputValueChanged_ColdDown()
    {
        float val = 10f;
        if (!float.TryParse(_inputColdDown.text, out val))
        {
            val = 10f;
        }
        m_fColdDown = val;
    }


    public void OnInputValueChanged_LastTime()
    {
        float val = 5f;
        if (!float.TryParse(_inputLastTime.text, out val))
        {
            val = 10f;
        }
        m_fPopoLastTime = val;
    }

    public void OnInputValueChanged_MaxEquipedNum()
    {
        int val = 5;
        if (!int.TryParse(_inputMaxEquippedNum.text, out val))
        {
            val = 5;
        }
        m_nAvailableNum = val;
    }

    public void OnInputValueChanged_SmallColdDown()
    {
        float val = 3f;
        if (!float.TryParse(_inputSmallColdDown.text, out val))
        {
            val = 3f;
        }
        m_fSmallColdDown = val;
    }

    public void Save(XmlDocument xmlDoc, XmlNode node)
    {
        StringManager.CreateNode(xmlDoc, node, "NumOneTime", m_nMaxSelectedNum.ToString() );
        StringManager.CreateNode(xmlDoc, node, "ColdDown", m_fColdDown.ToString( "f1" ) );
        StringManager.CreateNode(xmlDoc, node, "LastTime", m_fPopoLastTime.ToString("f1"));
        StringManager.CreateNode(xmlDoc, node, "MaxEquippedNum", m_nAvailableNum.ToString());
        StringManager.CreateNode(xmlDoc, node, "SmallColdDown", m_fSmallColdDown.ToString());
    }

    public void Load(XmlNode node)
    {
        if ( node == null || node.ChildNodes == null )
        {
            return;
        }

        for ( int i = 0; i < node.ChildNodes.Count; i++ )
        {
            XmlNode sub_node = node.ChildNodes[i];
            switch( sub_node.Name )
            {
                case "NumOneTime":
                    {
                        if ( !int.TryParse(sub_node.InnerText, out m_nMaxSelectedNum ) )
                        {
                            m_nMaxSelectedNum = 1;
                        }
                    }
                    break;

                case "ColdDown":
                    {
                        if (!float.TryParse(sub_node.InnerText, out m_fColdDown))
                        {
                            m_fColdDown = 10f;
                        }
                    }
                    break;
                case "LastTime":
                    {
                        if (!float.TryParse(sub_node.InnerText, out m_fPopoLastTime))
                        {
                            m_fPopoLastTime = 5f;
                        }
                    }
                    break;
                case "SmallColdDown":
                    {
                        if (!float.TryParse(sub_node.InnerText, out m_fSmallColdDown))
                        {
                            m_fSmallColdDown = 3f;
                        }
                    }
                    break;
                case "MaxEquippedNum":
                    {
                        if (!int.TryParse(sub_node.InnerText, out m_nAvailableNum))
                        {
                            m_nAvailableNum = 5;
                        }
                    }
                    break;
            }
        }
        InitBoughtGestures();
        UpdateEditorUI();
    }

    void UpdateEditorUI()
    {
        _inputColdDown.text = m_fColdDown.ToString( "f1" );
        _inputGestureNumOneTime.text = m_nMaxSelectedNum.ToString();
        _inputLastTime.text = m_fPopoLastTime.ToString( "f1" );
        _inputSmallColdDown.text = m_fSmallColdDown.ToString("f1");
        _inputMaxEquippedNum.text = m_nAvailableNum.ToString();
    }
}
