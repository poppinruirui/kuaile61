﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CBuyResult : MonoBehaviour {

    public static CBuyResult s_Instance = null;

    public GameObject _goContainerAll;
    public Text _txtInfo;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show( string szInfo )
    {
        _goContainerAll.SetActive(true);
        _txtInfo.text = szInfo;
    }

    public void Hide()
    {
        _goContainerAll.SetActive( false);
    } 
}
