﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CSelectRoomManager : MonoBehaviour {

    public GameObject _panelSelectRoom;
    public CCommonJinDuTiao _jindutiao;

    public Toggle _toggleEnterTestRoom;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickButton_0()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test( 0 );
        ShowJinDuTiao();
    }

    public void OnClickButton_1()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(1);
        ShowJinDuTiao();
    }


    public void OnClickButton_2()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(2);
        ShowJinDuTiao();
    }

    public void ShowJinDuTiao()
    {
        _jindutiao.gameObject.SetActive( true );
        _jindutiao.SetInfo( "正在登入游戏" );
    }

    public void OnClickButton_PrevPage()
    {
        SetSelectRoomPanelVisible(false);

    }

    public void OnClickButton_EnterSelectRoomPanel()
    {
        SetSelectRoomPanelVisible( true );
    }

    public void SetSelectRoomPanelVisible( bool bVisible )
    {
        _panelSelectRoom.SetActive(bVisible);
    }

    public static bool s_bEnterTestRoom = false;
    public void OnToggleValueChanged_EnterTestRoom()
    {
        s_bEnterTestRoom = _toggleEnterTestRoom.isOn;
    }
}
