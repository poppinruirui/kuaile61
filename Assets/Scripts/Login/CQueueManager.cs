﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CQueueManager : Photon.PunBehaviour{

    public static CQueueManager s_Instance;

    public GameObject _panelQueue;

    public GameObject m_preQueueItem;

    public InputField _inputPlayerId;

    public CCyberTreeList m_lstPlayers;

    public CQueueItem m_itemMainPlayer;

    public Text _txtWaitingTime;
    public Text _txtTotalPlayerNum;
    public Text _txtCurPlayerNum;

    public int m_nTotalPlayerNum = 3;
    

    float m_fStartQueueTime = 0f;

    bool m_bQueue = false;

    public float m_fEnterArenaCoutingTime = 5f;

    bool m_bQueueFinished = false;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
      
          
    }

    public bool IsQueueFinished()
    {
        return m_bQueueFinished;
    }

    public void StartQueue()
    {
        _panelQueue.SetActive(true);

        m_itemMainPlayer.SetPlayerId( PhotonNetwork.player.ID );
          m_itemMainPlayer.SetPlayerName(PhotonNetwork.player.NickName);
          m_itemMainPlayer.SetSkinId(PhotonNetwork.player.ID,ShoppingMallManager.GetCurEquipedSkinId() );

          if ( PhotonNetwork.isMasterClient )
          {
              m_fStartQueueTime = (float)PhotonNetwork.time;
          }

          FuckMe();

          _txtTotalPlayerNum.text = MapEditor.GetPlayerNumToStartGame().ToString();
    }

    List<PhotonPlayer> m_lstSortedPhotonPlayer = new List<PhotonPlayer>();

    Dictionary<int, int> m_dicPlayerId2SkinId = new Dictionary<int, int>();

    public void EndQueue()
    {
        _panelQueue.SetActive(false);
        m_bQueueFinished = true;

        //  CAudioManager.s_Instance.audio_login_bgm.Stop();
        //  CAudioManager.s_Instance.audio_main_bg.Play();
        CAudioManager.s_Instance.StopMainBg(CAudioManager.eMainBg.login);
        CAudioManager.s_Instance.PlayMainBg(CAudioManager.eMainBg.arena);
    }

    void RefreshPlayerList()
    {
        if (m_bQueueFinished)
        {
            return;
        }

        if (PhotonNetwork.playerList.Length >= MapEditor.GetPlayerNumToStartGame())
        {
            EndQueue();
            return;
        }

        if ( !m_bQueue)
        {
            return;
        }

        List<CCyberTreeListItem> lst = null;
        m_lstPlayers.GetItemList(ref lst);
        for (int i = 0; i < lst.Count; i++)
        {
            CCyberTreeListItem item = lst[i];
            DeleteItem( (CQueueItem)item );
        }
        m_lstPlayers.ClearItems();

        m_lstSortedPhotonPlayer.Clear();
        bool bFirst = true;
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonPlayer phPlayer = PhotonNetwork.playerList[i];
            if (phPlayer.ID == PhotonNetwork.player.ID)
            {
                continue;
            }
            if (bFirst)
            {
                m_lstSortedPhotonPlayer.Add(phPlayer);
                bFirst = false;
                continue;
            }
            bool bInserted = false;
            for ( int j = 0; j < m_lstSortedPhotonPlayer.Count; j++ )
            {
                PhotonPlayer node = m_lstSortedPhotonPlayer[j];
                if (phPlayer.ID < node.ID)
                {
                    m_lstSortedPhotonPlayer.Insert(j, phPlayer);
                    bInserted = true;
                    break;
                }
            } // end j
            if ( !bInserted)
            {
                m_lstSortedPhotonPlayer.Add(phPlayer);
            }

        } // end i

        for (int i = 0; i < m_lstSortedPhotonPlayer.Count; i++)
        {
            PhotonPlayer phPlayer = m_lstSortedPhotonPlayer[i];
            CQueueItem item = NewItem();
            item.SetPlayerId(phPlayer.ID);
            item.SetPlayerName(phPlayer.NickName);
            int nSkinId = 0;
            if ( !m_dicPlayerId2SkinId.TryGetValue( phPlayer.ID, out nSkinId ) )
            {
                nSkinId = 0;
            }
            item.SetSkinId(phPlayer.ID, nSkinId);
            m_lstPlayers.AddItem(item);
        }
    }

    // Update is called once per frame
    const float c_fRefreshPlayerListInterval = 1f;
    float m_fRefreshPlayerListTimeElapse = 0f;
    void Update () {

        return; // poppin test

        m_fRefreshPlayerListTimeElapse += Time.deltaTime;
        if (m_fRefreshPlayerListTimeElapse >= c_fRefreshPlayerListInterval)
        {
            RefreshPlayerList();
            UpdateWaitingTime();
            _txtCurPlayerNum.text = PhotonNetwork.playerList.Length.ToString();


            m_fRefreshPlayerListTimeElapse = 0f;
        }
        
    }


    List<CQueueItem> m_lstRecycleditems = new List<CQueueItem>();
    public CQueueItem NewItem()
    {
        CQueueItem item = null;
        if (m_lstRecycleditems.Count > 0)
        {
            item = m_lstRecycleditems[0];
            item.gameObject.SetActive( true );
            m_lstRecycleditems.RemoveAt(0);
        }
        else
        {
            item = GameObject.Instantiate(m_preQueueItem).GetComponent<CQueueItem>();
        }
        return item;
    }

    public void DeleteItem(CQueueItem item )
    {
        item.gameObject.SetActive(false);
        m_lstRecycleditems.Add(item);
    }

    int m_nShit = 2;
    public void OnButtonClick_AddOnePlayerToList( )
    {
        int nPlayerId = m_nShit++;
        CQueueItem item = GameObject.Instantiate(m_preQueueItem).GetComponent<CQueueItem>();
        item.SetPlayerId(nPlayerId);
        m_lstPlayers.AddItem(item);
    }

    
    public void FuckMe()
    {
        photonView.RPC("PRC_FuckMe", PhotonTargets.All, PhotonNetwork.player.ID, ShoppingMallManager.GetCurEquipedSkinId() );
    }

    [PunRPC]
    public void PRC_FuckMe( int nNewLoginInPlayerId, int nEquipedSkinId )
    {
        if ( PhotonNetwork.isMasterClient )
        {
            SyncStartQueueTime();
        }

        photonView.RPC("RPC_SyncMySkinId", PhotonTargets.All, PhotonNetwork.player.ID, ShoppingMallManager.GetCurEquipedSkinId());
    }

    [PunRPC]
    public void RPC_SyncMySkinId( int nPlayerId, int nSkinId )
    {
        m_dicPlayerId2SkinId[nPlayerId] = nSkinId;
    }

    public void SyncStartQueueTime()
    {
        photonView.RPC("RPC_SyncStartQueueTime", PhotonTargets.All, m_fStartQueueTime );
    }

    [PunRPC]
    public void RPC_SyncStartQueueTime( float fStartQueueTime)
    {
        m_fStartQueueTime = fStartQueueTime;

        m_bQueue = true;
    }

    void UpdateWaitingTime()
    {
        float fWaitingTime = (float)PhotonNetwork.time - m_fStartQueueTime;
        float fMin = 0;
        float fSec = 0;
        string szMin = "";
        string szSec = "";
        if (fWaitingTime <= 60f)
        {
            fSec = fWaitingTime % 60f;
            if ( fSec < 10 )
            {
                _txtWaitingTime.text = "00 : 0" + fSec.ToString("f0");
            }
            else
            {
                _txtWaitingTime.text = "00 : " + fSec.ToString("f0");
            }
            
        }
        else
        {
            fMin = fWaitingTime / 60f;
            fSec = fWaitingTime % 60f;
            if (fMin > 10)
            {
                szMin = fMin.ToString( "f0" );
            }
            else
            {
                szMin = "0" + fMin.ToString("f0");
            }

            if (fSec > 10)
            {
                szSec = fSec.ToString( "f0" );
            }
            else
            {
                szSec = "0" + fSec.ToString("f0");
            }
            _txtWaitingTime.text = szMin + " : " + szSec;
        }
      
        
    }

    public void OnClickButton_GuDaoKaiJu()
    {
        photonView.RPC( "RPC_MasterPleaseStartTheGame", PhotonTargets.All);
    }

    [PunRPC]
    public void RPC_MasterPleaseStartTheGame()
    {
        EndQueue();
    }
}
