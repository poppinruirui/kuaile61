using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum ResourceType:sbyte
{
    ResourceData,
    ResourceText,
    ResourceBundleList,
    ResourceBundle
};

public class Resource
{
    public string           name;
    public string           url;
    public ResourceType     type;
    public string           text;    
    public byte[]           data;
    
    public Resource(string name, string url, ResourceType type = ResourceType.ResourceData)
    {
        this.name = name;
        this.url = url;
        this.type = type;
        this.text = "";
        this.data = null;
    }
    public virtual void setText(string text)
    {
        this.text = text;
    }
    public virtual void setData(byte[] data)
    {
        this.data = data;
    }
    public virtual void clear()
    {
    }
};

public class ResourceBundleList : Resource
{
    public AssetBundle         bundle;
    public AssetBundleManifest manifest;
    
    public ResourceBundleList(string name, string url):
        base(name, url, ResourceType.ResourceBundleList)
    {
        this.bundle = null;
        this.manifest = null;
    }

    public AssetBundleManifest getManifest()
    {
        if (this.bundle != null && this.manifest == null) {
            this.manifest = this.bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
        }
        return this.manifest;
    }
    public override void clear()
    {
        if (this.bundle != null) {
            this.bundle.Unload(true);
        }
    }
};

public class ResourceBundle : Resource
{
    public string      hash;
    public AssetBundle bundle;
    public Shader      shader;
    public Sprite      sprite;
    public Material    material;
    
    public ResourceBundle(string name, string url):
        base(name, url, ResourceType.ResourceBundle)
    {
        this.hash   = null;
        this.bundle = null;
        this.shader = null;
        this.material = null;
    }

    public string getFileSavePath()
    {
        return Utils.urlToFilePath(Application.persistentDataPath + "/" + Utils.bundleFilePath(this.url));
    }

    public string getHashSavePath()
    {
        return Utils.urlToFilePath(Application.persistentDataPath + "/" + Utils.bundleHashPath(this.url));
    }

    public Sprite getSprite()
    {
        if (this.bundle != null && this.sprite == null) {
            this.sprite = this.bundle.LoadAsset<Sprite>(Utils.bundlePathToRelativeName(this.url));
        }
        return this.sprite;
    }

    public Material getSpriteMaterial()
    {
        if (this.material == null) {
            Sprite sprite = this.getSprite();
            this.material = new Material(Shader.Find("Sprites/Default"));
            this.material.mainTexture = sprite.texture;
        }
        return this.material;
    }
    
    public Shader getShader()
    {
        if (this.bundle != null && this.shader == null) {
            this.shader = this.bundle.LoadAsset<Shader>(Utils.bundlePathToRelativeName(this.url));
        }
        return this.shader;
    }

    public Material getShaderMaterial()
    {
        if (this.material == null) {
            Shader shader = this.getShader();
            this.material = new Material(shader);
        }
        return this.material;
    }
    
    public override void clear()
    {
        if (this.bundle != null) {
            this.bundle.Unload(true);
        }
    }    
};

public class ProgressEventArgs : EventArgs
{
    public ProgressEventArgs(ResourceLoader loader, Resource resource)
    {
        this.loader = loader;
        this.resource = resource;
    }    
    public ResourceLoader loader;
    public Resource       resource;
};

public enum LoadResult:sbyte
{
    Success = 0,
    Failed
};

public class LoadedEventArgs : EventArgs
{
    public LoadedEventArgs(ResourceLoader loader, LoadResult result)
    {
        this.loader = loader;
        this.result = result;
    }    
    public ResourceLoader loader;
    public LoadResult     result;
};

public abstract class ResourceLoader
{
    public string url;
    public Dictionary<string, Resource> resources;
    public int progress;
    public event EventHandler<ProgressEventArgs> raiseProgressEvent;
    public event EventHandler<LoadedEventArgs> raiseLoadedEvent;

    public ResourceLoader(string url)
    {
        this.url = url;        
        this.resources = new Dictionary<string, Resource>();
    }

    public void clear()
    {
        this.raiseProgressEvent = null;
        this.raiseLoadedEvent = null;        
        foreach (var pair in this.resources) {
            Resource resource = pair.Value;
            resource.clear();
        }
        this.resources.Clear();
    }
    
    public void add(Resource[] resources)
    {
        for (var i=0; i<resources.Length; ++i) {
            Resource resource = resources[i];     
            this.resources.Add(resource.name, resource);
        }
    }

    public abstract IEnumerator load();

    public void raiseProgress(Resource resource)
    {
        if (this.raiseProgressEvent != null) {
            this.raiseProgressEvent(this, new ProgressEventArgs(this, resource));
        }        
    }

    public void raiseLoaded(int counter)
    {
        if (this.raiseLoadedEvent != null) {
            this.raiseLoadedEvent(this, new LoadedEventArgs(this, counter == this.resources.Count ? LoadResult.Success : LoadResult.Failed));
        }        
    }
};

public class LocaleResourceLoader : ResourceLoader
{
    public LocaleResourceLoader(string url) :
        base(url)
    {
    }

    public bool isWebRequestNeed()
    {
        return this.url.Contains("://");
    }
    
    public override IEnumerator load()
    {
        if (Debug.isDebugBuild) {        
            Debug.Log("load from " + this.url + " ...");
        }
        AsyncOperation asyncOperation = null;
        UnityWebRequest webRequest = null;
        AssetBundleCreateRequest fileRequest = null;
        ResourceBundleList resourceBundleList = (this.resources[Utils.platformBundleListPath()] as ResourceBundleList);
        string url = this.url + "/" + resourceBundleList.url;
        if (Debug.isDebugBuild) {
            Debug.Log("get " + url);
        }
        if (this.isWebRequestNeed()) {
            webRequest = UnityWebRequest.GetAssetBundle(url, 0);
            asyncOperation = webRequest.SendWebRequest();
        }
        else {
            fileRequest = AssetBundle.LoadFromFileAsync(url);
            asyncOperation = fileRequest;
        }
        yield return asyncOperation;
        if (this.isWebRequestNeed()) {
            resourceBundleList.bundle = (webRequest.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
        }
        else {
            resourceBundleList.bundle = fileRequest.assetBundle;
        }
        if (resourceBundleList.bundle == null) {
            if (Debug.isDebugBuild) {        
                Debug.Log("load from " + url + " failed!");
            }
        }
        else {
            AssetBundleManifest manifest = resourceBundleList.getManifest();
            string[] bundleNames = manifest.GetAllAssetBundles();
            for (var i=0; i<bundleNames.Length; ++i) {
                if (Debug.isDebugBuild) {        
                    Debug.Log("add bundle " + bundleNames[i]);
                }                
                this.resources.Add(bundleNames[i], new ResourceBundle(bundleNames[i], Utils.platformBundlePath() + "/" + bundleNames[i]));
            }
            int counter = 0;
            ResourceBundle resourceBundle = null;
            foreach (var pair in this.resources) {
                Resource resource = pair.Value;
                url = this.url + "/" + resource.url;
                if (Debug.isDebugBuild) {
                    Debug.Log("get " + url);
                }
                if (resource.type == ResourceType.ResourceBundle) {
                    resourceBundle = (resource as ResourceBundle);
                    resourceBundle.hash = manifest.GetAssetBundleHash(Utils.bundlePathToName(resource.url)).ToString();
                    string hashSavePath = resourceBundle.getHashSavePath();
                    string fileSavePath = resourceBundle.getFileSavePath();
                    if (File.Exists(hashSavePath)) {
                        string hash = File.ReadAllText(hashSavePath);
                        if (hash == resourceBundle.hash && File.Exists(fileSavePath)) {
                            this.progress = (++counter * 100 / this.resources.Count);
                            this.raiseProgress(resource);
                            continue;
                        }
                    }
                    #if UNITY_IOS
                    UnityEngine.iOS.Device.SetNoBackupFlag(hashSavePath);
                    UnityEngine.iOS.Device.SetNoBackupFlag(fileSavePath);
                    #endif
                    if (this.isWebRequestNeed()) { 
                        webRequest = UnityWebRequest.Get(url);
                        webRequest.downloadHandler = new DownloadHandlerFile(fileSavePath);
                        asyncOperation = webRequest.SendWebRequest();
                    }
                    else {
                        fileRequest = AssetBundle.LoadFromFileAsync(url);
                        asyncOperation = fileRequest;
                    }
                    yield return asyncOperation;
                }
                else {
                    this.progress = (++counter * 100 / this.resources.Count);
                    this.raiseProgress(resource);
                    continue;
                }
                if (this.isWebRequestNeed()) {
                    if (webRequest.isNetworkError || webRequest.isHttpError) {
                        if (Debug.isDebugBuild) {
                            Debug.Log("get " + resource.url + " failed with: " + webRequest.error);
                        }
                        break;
                    }
                    resourceBundle = resource as ResourceBundle;
                    string fileSavePath = resourceBundle.getFileSavePath();
                    if (File.Exists(fileSavePath)) {
                        resourceBundle.bundle = AssetBundle.LoadFromFile(fileSavePath);
                        if (resourceBundle.bundle == null) {
                            if (Debug.isDebugBuild) {
                                Debug.Log("load bundle " + fileSavePath + "failed!");
                            }
                            break;
                        }
                        string hashSavePath = resourceBundle.getHashSavePath();
                        File.WriteAllText(hashSavePath, resourceBundle.hash);
                    }
                }
                else {
                    if (fileRequest.assetBundle == null) {
                        if (Debug.isDebugBuild) {
                            Debug.Log("get " + resource.url + " failed!");
                        }
                        break;
                    }
                    resourceBundle = resource as ResourceBundle;
                    resourceBundle.bundle = fileRequest.assetBundle;
                    string fileSavePath = resourceBundle.getFileSavePath();
                    try {
                        File.Copy(url, fileSavePath, true);
                    }
                    catch (Exception e) {
                        if (Debug.isDebugBuild) {
                            Debug.Log("copy " + url + " to " + fileSavePath + "failed: " + e + "!");
                        }
                        break;
                    }
                    string hashSavePath = resourceBundle.getHashSavePath();
                    File.WriteAllText(hashSavePath, resourceBundle.hash);
                }
                this.progress = (++counter * 100 / this.resources.Count); 
                this.raiseProgress(resource);
            }
            this.raiseLoaded(counter);
        }
    }
};

public class RemoteResourceLoader : ResourceLoader
{    
    public RemoteResourceLoader(string url) :
        base(url)
    {
    }

    public override IEnumerator load()
    {
        if (Debug.isDebugBuild) {        
            Debug.Log("load from " + this.url + " ...");
        }
        int counter = 0;
        ResourceBundleList resourceBundleList = null;
        ResourceBundle resourceBundle = null;
        foreach (var pair in this.resources) {
            Resource resource = pair.Value;
            string url = this.url + '/' + resource.url;
            if (Debug.isDebugBuild) {            
                Debug.Log("get " + url);
            }
            UnityWebRequest request = null;
            if (resource.type == ResourceType.ResourceBundleList) {
                resourceBundleList = (resource as ResourceBundleList);
                request = UnityWebRequest.GetAssetBundle(url, 0);
                yield return request.SendWebRequest();
            }
            else if (resource.type == ResourceType.ResourceBundle && resourceBundleList != null) {
                AssetBundleManifest manifest = resourceBundleList.getManifest();
                resourceBundle = (resource as ResourceBundle);
                resourceBundle.hash = manifest.GetAssetBundleHash(Utils.bundlePathToName(resource.url)).ToString();
                string hashSavePath = resourceBundle.getHashSavePath();
                string fileSavePath = resourceBundle.getFileSavePath();
                if (File.Exists(hashSavePath)) {
                    string hash = File.ReadAllText(hashSavePath);
                    if (hash == resourceBundle.hash && File.Exists(fileSavePath)) {
                        resourceBundle.bundle = AssetBundle.LoadFromFile(fileSavePath);
                        if (resourceBundle.bundle == null) {
                            if (Debug.isDebugBuild) {
                                Debug.Log("load bundle " + fileSavePath + "failed!");
                            }
                        }
                        this.progress = (++counter * 100 / this.resources.Count);
                        this.raiseProgress(resource);
                        continue;
                    }
                }
 #if UNITY_IOS
                UnityEngine.iOS.Device.SetNoBackupFlag(hashSavePath);
                UnityEngine.iOS.Device.SetNoBackupFlag(fileSavePath);
#endif                
                request = UnityWebRequest.Get(url);
                request.downloadHandler = new DownloadHandlerFile(fileSavePath);
                yield return request.SendWebRequest();
            }
            else {
                request = UnityWebRequest.Get(url);
                yield return request.SendWebRequest();
            }
            if (request.isNetworkError || request.isHttpError) {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get " + resource.url + " failed with: " + request.error);
                }
                break;
            }
            else {
                if (resource.type == ResourceType.ResourceBundleList) {
                    resourceBundleList = resource as ResourceBundleList;
                    resourceBundleList.bundle = (request.downloadHandler as DownloadHandlerAssetBundle).assetBundle;
                }
                else if (resource.type == ResourceType.ResourceBundle) {
                    resourceBundle = resource as ResourceBundle;
                    string fileSavePath = resourceBundle.getFileSavePath();
                    if (File.Exists(fileSavePath)) {
                        resourceBundle.bundle = AssetBundle.LoadFromFile(fileSavePath);
                        if (resourceBundle.bundle == null) {
                            if (Debug.isDebugBuild) {                            
                                Debug.Log("load bundle " + fileSavePath + "failed!");
                            }
                        }                        
                        string hashSavePath = resourceBundle.getHashSavePath();
                        File.WriteAllText(hashSavePath, resourceBundle.hash);
                    }
                }
                else if (resource.type == ResourceType.ResourceData) {
                    resource.setData(request.downloadHandler.data);
                }
                else if (resource.type == ResourceType.ResourceText) {
                    resource.setText(request.downloadHandler.text);
                }
            }
            this.progress = (++counter * 100 / this.resources.Count); 
            this.raiseProgress(resource);            
        }
        this.raiseLoaded(counter);
    }
};
