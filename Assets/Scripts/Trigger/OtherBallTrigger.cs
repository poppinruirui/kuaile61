﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherBallTrigger : MonoBehaviour
{
    public Ball _ball;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Dust")
        {
            if ( _ball.IsEjecting() && !_ball._Player.IsSneaking()/* && !_ball.IsIgnoreDust()*/)
            {
                _ball.SlowDownEject();
            }
        }
    }
}
