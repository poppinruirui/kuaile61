﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosGunSight : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public GameObject m_goHead;
    public GameObject m_goJoint;
    public GameObject m_goStick;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateStatus(Vector3 pos, Vector2 dir, float fMotherRadius, float fChildSize, float fDistanceMultiple)
    {
        float fAngle = CyberTreeMath.Dir2Angle(dir.x, dir.y) - 90f;
        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, fAngle);

        this.transform.position = pos;

        float fTotalDistance = Ball.GetRunDistance(fDistanceMultiple, fMotherRadius);

        float fDistanceForStick = fTotalDistance - fChildSize;
        vecTempScale.x = fChildSize;
        vecTempScale.y = fDistanceForStick;
        vecTempScale.z = 0f;
        m_goStick.transform.localScale = vecTempScale;

        vecTempScale.x = fChildSize;
        vecTempScale.y = fChildSize;
        vecTempScale.z = 0f;
        m_goJoint.transform.localScale = vecTempScale;
        vecTempPos.x = 0;
        vecTempPos.y = fDistanceForStick;
        vecTempPos.z = 0;
        m_goJoint.transform.localPosition = vecTempPos;

        vecTempScale.x = fChildSize;
        vecTempScale.y = fChildSize;
        vecTempScale.z = 0f;
        m_goHead.transform.localScale = vecTempScale;
        vecTempPos.x = 0;
        vecTempPos.y = fTotalDistance;
        vecTempPos.z = 0;
        m_goHead.transform.localPosition = vecTempPos;


    }
}
