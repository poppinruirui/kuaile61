﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
  九宫格编号分布示意图：
   1 2 3
   8 0 4
   7 6 5
*/


public class CStripeManager : MonoBehaviour {

    public static CStripeManager s_Instance = null;


    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempPos1 = new Vector3();
    static Vector3 vecTempPos2 = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Vector2 vecTempDir = new Vector2();
    

    public GameObject m_preBoWenGroup;
    public GameObject m_preBoWenGrid;
    public GameObject m_preBoWen;

    List<CBoWenGrid> m_lstGroups = new List<CBoWenGrid>();

    public Color m_colorBoWen;

    float m_fCurGridScale = 0;
    public float m_fGridCam2ScaleXiShu = 3.333f;
    float m_fCurCamSize = 0f;

    Vector3 m_vecOriginPoints = new Vector3( 0f, 0f );
    float[] m_aryThreshold = new float[MAX_THRESHOLD_NUM];
    float m_fLastCamSize = -1;

    public const int MAX_THRESHOLD_NUM = 3000;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        for (int i = 0; i < MAX_THRESHOLD_NUM; i++)
        {
            m_aryThreshold[i] = 0.1f * (float)(i + 1); // [to youhua]这里可以用二分查找法优化
        }

        Color color = Color.white;
        color.a = 0.1f;
        for (int i = 0; i < 9; i++) // “九宫格”机制
        {
            CBoWenGrid group = GameObject.Instantiate(m_preBoWenGrid).GetComponent<CBoWenGrid>();
            group.transform.SetParent( this.transform );
            m_lstGroups.Add(group);
        }
    }

    // Update is called once per frame
    void Update () {
        UpdateGridStatus();



    }

    void ChangeLevel( int nLevel )
    {
        m_fCurCamSize = m_aryThreshold[nLevel];
        float fLastDis = Vector2.Distance(GetMainCameraPos(), GetGridPos(0));
        float fLastScale = m_fCurGridScale;
        if (fLastScale == 0)
        {
            m_fCurGridScale = GetScaleByCamSize(m_fCurCamSize);
        }
        else
        {
            vecTempDir = GetGridPos(0) - GetMainCameraPos();
            vecTempDir.Normalize();
            m_fCurGridScale = GetScaleByCamSize(m_fCurCamSize); //  m_fGridCam2ScaleXiShu * m_fCurCamSize;
            float fNewDis = fLastDis * m_fCurGridScale / fLastScale;
            m_vecOriginPoints.x = GetMainCameraPos().x + fNewDis * vecTempDir.x;
            m_vecOriginPoints.y = GetMainCameraPos().y + fNewDis * vecTempDir.y;
            m_vecOriginPoints.z = 0;
            SetGridPos(0, m_vecOriginPoints);

        }
    }

    float GetScaleByCamSize( float fCamSize )
    {
        return m_fGridCam2ScaleXiShu * Mathf.Sqrt(m_fCurCamSize);
    }

    Vector3 GetGridPos( int nIndex )
    {
        return m_lstGroups[nIndex].transform.localPosition;
    }

    void SetGridPos( int nIndex, Vector3 pos )
    {
        m_lstGroups[nIndex].transform.localPosition = pos;
    }

    void UpdateGridStatus()
    {
        float fCamSize = GetMainCameraSize();
        for (int i = m_aryThreshold.Length - 1; i >= 1; i--)
        {
            if (m_fLastCamSize < m_aryThreshold[i] && fCamSize >= m_aryThreshold[i])
            {
                ChangeLevel(i);
                break;
            }

            if (m_fLastCamSize >= m_aryThreshold[i] && fCamSize < m_aryThreshold[i])
            {
                ChangeLevel(i - 1);
                break;
            }
        }
        m_fLastCamSize = fCamSize;


        vecTempPos = GetMainCameraPos();
        float fTempX =  ( vecTempPos.x - m_vecOriginPoints.x ) / m_fCurGridScale;
        float fTempY =  ( vecTempPos.y - m_vecOriginPoints.y ) / m_fCurGridScale;
        if (fTempX > 0)
        {
            fTempX += 0.5f;
        }
        else
        {
            fTempX -= 0.5f;
        }

        if (fTempY > 0)
        {
            fTempY += 0.5f;
        }
        else
        {
            fTempY -= 0.5f;
        }
        int nCX = (int)fTempX;
        int nCY = (int)fTempY;

        for ( int i = 0; i < m_lstGroups.Count; i++ )
        {
            CBoWenGrid group = m_lstGroups[i];
            int nX = 0;
            int nY = 0;
            switch ( i )
            {
                case 0: // C位
                    {
                        nX = nCX;
                        nY = nCY;
                     }
                    break;
                case 1: 
                    {
                        nX = nCX - 1;
                        nY = nCY + 1;
                    }
                    break;
                case 2: 
                    {
                        nX = nCX;
                        nY = nCY + 1;
                    }
                    break;
                case 3:
                    {
                        nX = nCX + 1;
                        nY = nCY + 1;
                    }
                    break;
                case 4:
                    {
                        nX = nCX + 1;
                        nY = nCY;
                    }
                    break;
                case 5:
                    {
                        nX = nCX + 1;
                        nY = nCY - 1;
                    }
                    break;
                case 6:
                    {
                        nX = nCX;
                        nY = nCY - 1;
                    }
                    break;
                case 7:
                    {
                        nX = nCX - 1;
                        nY = nCY - 1;
                    }
                    break;
                case 8:
                    {
                        nX = nCX - 1;
                        nY = nCY;
                    }
                    break;

            } // end switch

            vecTempPos.x = m_vecOriginPoints.x + m_fCurGridScale * nX;
            vecTempPos.y = m_vecOriginPoints.y + m_fCurGridScale * nY;
            group.SetPos(vecTempPos);
            group.SetScale(m_fCurGridScale);
        }
    }

    public float GetMainCameraSize()
    {
        return Camera.main.orthographicSize;
    }

    public Vector3 GetMainCameraPos()
    {
        return Camera.main.transform.position;
    }
}
