﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBloodStainManager : MonoBehaviour {

    public static CBloodStainManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();

    public GameObject[] m_aryBloodStainPrefab;
    public float[] m_aryBloodSize2BallSize;
    public float[] m_aryLastTime;
    public float[] m_aryFadeTime;
    public float m_fDynamicScaleTime = 0.5f;

    public GameObject m_preBloodTile;
    public float m_fBloodTileInterval = 2.5f;

    public GameObject m_preStain;
    public GameObject m_goArea0;

    public enum eBloodStainType
    {
        dead_spray,
        dead_static,
        explode,
        spit,
    };

    public enum eDynamicEffectType
    {
        none,
        scale,
        fill,
    }

    public enum eFadeMode
    {
        common,
        fill,
    }

    void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {
       

    }
	
	// Update is called once per frame
	void Update () {
        FadingLoop();
    }

    Dictionary<eBloodStainType, List<CBloodStain>> m_dicRecycledBloodStain = new Dictionary<eBloodStainType, List<CBloodStain>>();
    public CBloodStain NewBloodStain(eBloodStainType eType )
    {
        CBloodStain stain = null;
        List<CBloodStain> lst = null;
        if ( !m_dicRecycledBloodStain.TryGetValue( eType, out lst ) )
        {
            lst = null;
        }
        if ( lst != null && lst.Count > 0 )
        {
            stain = lst[0];
            stain.Reset();
            stain.gameObject.SetActive( true );
            lst.RemoveAt(0);
        }
        else
        {
            if ( lst == null )
            {
                lst = new List<CBloodStain>();
            }
            stain = GameObject.Instantiate( m_aryBloodStainPrefab[(int)eType] ).GetComponent<CBloodStain>();
            stain.transform.SetParent( this.transform );
        }
        stain.SetStainType( eType );
        m_dicRecycledBloodStain[eType] = lst;
        return stain;
    }

    public void DeleteBloodStain( CBloodStain stain )
    {
        List<CBloodStain> lst = null;
        eBloodStainType eType = stain.GetStainType();
        if (!m_dicRecycledBloodStain.TryGetValue(eType, out lst))
        {
            lst = null;
        }
        if (lst == null)
        {
            lst = new List<CBloodStain>();
        }
        stain.gameObject.SetActive( false );
        lst.Add(stain);
        m_dicRecycledBloodStain[eType] = lst;
    }

    public void AddBloodStain( Vector3 pos, Vector3 dir, float fScaleX, float fScaleY , eBloodStainType type, Color color, eDynamicEffectType eType = eDynamicEffectType.none, float fParam = 0, eFadeMode fade_mode = eFadeMode.common  )
    {
        CBloodStain stain = NewBloodStain(type);
        stain.SetDir(dir);
        stain.SetPos(pos);
        stain.setScale(fScaleX, fScaleY);
        stain.SetColor(color);
        stain.SetLastTime( m_aryLastTime[(int)type] );
        stain.BeginLast();
        stain.SetFadeTime( m_aryFadeTime[(int)type] );
        stain.SetFadeMode( fade_mode );
        switch ( eType )
        {
            case eDynamicEffectType.scale:
                {
                    stain.BeginDynamicScale( fParam);
                }
                break;
            case eDynamicEffectType.fill:
                {
                    stain.BeginFill( fParam );
                }
                break;
        }
    }

 
        

    public float GetScaleRatioByType(eBloodStainType type)
    {
        return m_aryBloodSize2BallSize[(int)type];
    }

    // =============================================== New
    Dictionary<uint, CBloodStain> m_dicBloodStains = new Dictionary<uint, CBloodStain>();

    public CBloodStain GetBloodStain_SprayMode(uint uKey)
    {
        CBloodStain stain = null;
        if (!m_dicBloodStains.TryGetValue(uKey, out stain))
        {
            stain = null;
        }
        return stain;
    }

    void AddBloodStain_SprayMode(uint uKey, CBloodStain stain)
    {
        m_dicBloodStains[uKey] = stain;
    }

    void RemoveBloodStain_SprayMode(uint uKey)
    {
        m_dicBloodStains.Remove(uKey);
    }

    List<CBloodStain> m_lstRecycledBloodStains = new List<CBloodStain>();
    public CBloodStain NewBloodStain_SprayMode(uint uKey)
    {
        CBloodStain stain = null;
        if (m_lstRecycledBloodStains.Count > 0)
        {
            stain = m_lstRecycledBloodStains[0];
            stain.SetActive(true);
            m_lstRecycledBloodStains.RemoveAt(0);
        }
        else
        {
            stain = GameObject.Instantiate(m_preStain).GetComponent<CBloodStain>();
            stain.transform.localRotation = Quaternion.identity;
            float fRotateAngle = Random.Range(0, 360);
            stain.transform.Rotate(0.0f, 0.0f, fRotateAngle);
            stain.transform.SetParent(m_goArea0.transform);
        }
        stain.SetKey(uKey);
        AddBloodStain_SprayMode(uKey, stain);
        return stain;
    }

    public void DeleteBloodStain_SprayMode(CBloodStain stain)
    {
        RemoveBloodStain_SprayMode(stain.GetKey());

        stain.SetActive(false);
        m_lstRecycledBloodStains.Add(stain);
    }

    List<CBloodStain> m_lstFading = new List<CBloodStain>();
    public void DoFade(CBloodStain stain)
    {
        RemoveBloodStain_SprayMode(stain.GetKey());
        m_lstFading.Add(stain);
    }

    public float m_fFadeTime = 3f;
    void FadingLoop()
    {
        float fAmount = Time.deltaTime / m_fFadeTime;

        for (int i = m_lstFading.Count - 1; i >= 0; i--)
        {
            CBloodStain stain = m_lstFading[i];
            stain.Fade(fAmount);
            if (stain.IsFadingCompleted())
            {
                stain.SetActive(false);
                m_lstRecycledBloodStains.Add(stain);

                m_lstFading.RemoveAt(i);
                continue;
            }
        }
    }

}
