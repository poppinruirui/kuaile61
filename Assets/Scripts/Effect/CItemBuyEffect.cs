﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CItemBuyEffect : MonoBehaviour {

    public CFrameAnimationEffect _effectMain;

    public const float DURATION = 0.5f;
    public const float SPEED = 150f;

    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();
    float m_fSpeed = 0f;

    Vector2 m_Dir = new Vector2();

    Vector3 m_vecDest = new Vector3();

    float m_fStartTime = 0f;

    public Ball _ballDest = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPos(Vector3 pos)
    {
        this.transform.position = pos;
    }

    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetSpeed( float fSpeed )
    {
        m_fSpeed = fSpeed;
    }

    public void SetScale(float fScale)
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetStartTime(float fStartTime )
    {
        m_fStartTime = fStartTime;
    }

    public float GetStartTime()
    {
        return m_fStartTime;
    }


    public void MoveToDest()
    {
        vecTempPos = GetPos();

        m_Dir = _ballDest.GetPos() - GetPos();
        m_Dir.Normalize();
        float fSpeed = m_fSpeed * Time.deltaTime;
        vecTempPos.x += fSpeed * m_Dir.x;
        vecTempPos.y += fSpeed * m_Dir.y;


        SetPos(vecTempPos);
    }
}
