﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosEffect : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetLocalPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }
}
