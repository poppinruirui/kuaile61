﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSkillModuleForBall : MonoBehaviour {

    public Ball _ball;

    public GameObject _goEffectContainer;
    CCosmosEffect[] m_aryActiveEffect_QianYao = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];
    CCosmosEffect[] m_aryActiveEffect_ChiXu = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];

    short[] m_arySkillStatus = new short[(int)CSkillSystem.eSkillId.total_num];

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetCurEffects(ref CCosmosEffect[] aryQianYao, ref CCosmosEffect[] aryChiXu)
    {
        aryQianYao = m_aryActiveEffect_QianYao;
        aryChiXu = m_aryActiveEffect_ChiXu;
    }

    public void ResetAllSkillStatus()
    {
        for ( int i = 0; i < m_arySkillStatus.Length; i++ )
        {
            m_arySkillStatus[i] = 0;
        }
    }

    public void SetCurSkillStatus(CSkillSystem.eSkillId eSkillId, short nStatus )
    {
        m_arySkillStatus[(int)eSkillId] = nStatus;
    }

    
    public short GetCurSkillStatus(CSkillSystem.eSkillId eSkillId)
    {
        return m_arySkillStatus[(int)eSkillId];
    }

    public void UpdateSkillStatus(CSkillSystem.eSkillId eSkillId, short nSkillStatus)
    {
       short nCurSkillStatus = GetCurSkillStatus(eSkillId);

       if (nCurSkillStatus  == nSkillStatus )
       {
            return;
       }

        if (nCurSkillStatus == 1 && nSkillStatus != 1 ) // 清除前摇特效
        {
            CCosmosEffect effect = m_aryActiveEffect_QianYao[(int)eSkillId];
            m_aryActiveEffect_QianYao[(int)eSkillId] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.qianyao);
        }

        if (nCurSkillStatus == 2 && nSkillStatus != 2)// 清除持续特效
        {
            CCosmosEffect effect = m_aryActiveEffect_ChiXu[(int)eSkillId];
            m_aryActiveEffect_ChiXu[(int)eSkillId] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.chixu);
        }


        if (nSkillStatus == 1)// 播放前摇特效
        {
            CCosmosEffect effect = CEffectManager.s_Instance.NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.qianyao);
            SetParent(effect, eSkillId);
            effect.SetLocalPos(Vector3.zero);
            effect.SetScale(CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.qianyao));
            CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
            frame_ani_effect.BeginPlay(false);
            m_aryActiveEffect_QianYao[(int)eSkillId] = effect;
        }

        if (nSkillStatus == 2) // 播放持续特效
        {
            CCosmosEffect effect = CEffectManager.s_Instance.NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.chixu);
            SetParent(effect, eSkillId);
            effect.SetLocalPos( Vector3.zero );
            effect.SetScale( CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.chixu) );
            CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
            frame_ani_effect.BeginPlay( true );
            m_aryActiveEffect_ChiXu[(int)eSkillId] = effect;
        }

      

        //// 以上为技能特效 

        //// 以下为一些逻辑功能
        switch(eSkillId)
        {
            case CSkillSystem.eSkillId.r_sneak:
                {
                    if ( _ball.IsMainPlayer())
                    {
                        if ( nSkillStatus == 0)
                        {
                            _ball.SetColliderDustEnable(true);

                        }
                        else
                        {
                            _ball.SetColliderDustEnable(false);
                        }
                    }
                }
                break;

        } // end switch
          //// end 逻辑功能


        // 设置最新的状态
        SetCurSkillStatus(eSkillId, nSkillStatus);
    }

    public void SetParent( CCosmosEffect efffect, CSkillSystem.eSkillId eSkillId)
    {
        efffect.transform.SetParent(_goEffectContainer.transform);
    }
}
