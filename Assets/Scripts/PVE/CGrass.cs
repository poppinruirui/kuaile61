﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGrass : CPveObj {

	static Color cTempColor = new Color ();

	List<Ball> m_lstBalls = new List<Ball>();
	bool m_bMainPlayerIn = false;

	CGrassEditor.sGrassConfig m_Config;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		m_fCheckMainPlayerInTimeCount += Time.deltaTime;
		if (m_fCheckMainPlayerInTimeCount >= c_fCheckMainPlayerInInterval) {
			CheckIfMainPlayerInMe ();
			m_fCheckMainPlayerInTimeCount = 0;
		}


        RecoveringAlpha ();
	}

	public void SetConfig( CGrassEditor.sGrassConfig config )
	{
		m_Config = config;
	}

	public CGrassEditor.sGrassConfig GetConfig()
	{
		return m_Config;
	}

	void OnMouseDown()
	{
		CPveEditor.s_Instance.SelectObj ( this );
	}
		
	public void RemoveBall( Ball ball )
	{
		m_lstBalls.Remove (ball);
        if (ball._Player && ball._Player.IsMainPlayer ()) {

		}
	}

	public void AddBall( Ball ball )
	{
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball the_ball = m_lstBalls[i];
            if ( ball == the_ball )
            {
                return;
            }
        }

		m_lstBalls.Add (ball);
		if (ball._Player && ball._Player.IsMainPlayer ()) { 
			ProcessMainPlayerEnter ();
		}
	}

	const float c_fCheckMainPlayerInInterval = 3f;
	float m_fCheckMainPlayerInTimeCount = 3f;
	bool CheckIfMainPlayerInMe()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball._Player && ball._Player.IsMainPlayer ()) {
				m_bMainPlayerIn = true;
				return m_bMainPlayerIn;
			}
		}

		if (!m_bRecoveringAlpha) {
			BeginRecoverAlpha ();
		}

		m_bMainPlayerIn = false;
		return m_bMainPlayerIn;
	}

	bool m_bRecoveringAlpha = false;
	void BeginRecoverAlpha()
	{
		m_bRecoveringAlpha = true;
	}

	void RecoveringAlpha()
	{
		if (!m_bRecoveringAlpha) {
			return;
		}


		if (_srMain.color.a >= 1f) {
			EndRecoverAlpha ();
		}

		cTempColor = _srMain.color;
		cTempColor.a += Time.deltaTime;
		if (cTempColor.a > 1f) {
			cTempColor.a = 1f;
		}
		_srMain.color = cTempColor;
	}

	void EndRecoverAlpha()
	{
		m_bRecoveringAlpha = false;
	}

	void ProcessMainPlayerEnter()
	{
		cTempColor = _srMain.color;
		cTempColor.a = 0.4f;
		_srMain.color = cTempColor;
		EndRecoverAlpha ();
	}

}
